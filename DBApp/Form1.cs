﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;


namespace DBApp
{
  public partial class Form1 : Form
  {
    private const string Oradb = "Provider=MSDAORA;Data Source=XE;User Id=system;Password=458824;";
    private OleDbConnection _connection;
    private OleDbDataAdapter _adapter;
    private DataSet _dataSet;

    public Form1()
    {
      InitializeComponent();
      this.CreateConnection();
    }

    private void CreateConnection()
    {
      _connection = new OleDbConnection(Oradb);
    }

    private void GetData()
    {
      _connection.Open();
      _dataSet = new DataSet();
      _adapter.Fill(_dataSet);

      dataGridView1.DataSource = _dataSet.Tables[0];

      _connection.Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.HomeSteaderAdapter(_connection);
      this.GetData();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      _connection.Open();

      _adapter.Update(_dataSet);

      _connection.Close();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.AreaStructureAdapter(_connection);
      this.GetData();
    }

    private void button4_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.StructureMaterialAdapter(_connection);
      this.GetData();
    }

    private void button5_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.StructurePurposeAdapter(_connection);
      this.GetData();
    }

    private void button6_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.SteaderPaymentsAdapter(_connection);
      this.GetData();
    }

    private void button7_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.TaxTypeAdapter(_connection);
      this.GetData();
    }

    private void button8_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.MonthTaxAdapter(_connection);
      this.GetData();
    }

    private void button9_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.CurrenAndPreviousValuesAdapter(_connection);
      this.GetData();
    }

    private void button10_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.YearTaxAdapter(_connection);
      this.GetData();
    }

    private void button11_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.ArrearsSumAdapter(_connection);
      this.GetData();
    }

    private void button12_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.PaidSumAdapter(_connection);
      this.GetData();
    }

    private void button13_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.WorstDebtorsAdapter(_connection);
      this.GetData();
    }

    private void button14_Click(object sender, EventArgs e)
    {
      _adapter = Adapters.HomeSteaderYearTaxesAdapter(_connection, textBox1.Text);
      this.GetData();
    }
    


  }
}
