﻿using System.Data;
using System.Data.OleDb;

namespace DBApp
{
  public static class Adapters
  {
    public static OleDbDataAdapter HomeSteaderAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM HomeSteader", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO HomeSteader (AreaNum, FirstName, Phone, AreaSize, IsWorking, SurName, FatherName) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE HomeSteader SET FirstName = ?, Phone = ?, AreaSize = ?, IsWorking = ?, SurName = ?, FatherName = ? " +
        "WHERE AreaNum = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM HomeSteader WHERE AreaNum = ?");

      adapter.DeleteCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@FirstName",
          OleDbType.VarChar, 20, "FirstName");
      adapter.UpdateCommand.Parameters.Add("@Phone",
          OleDbType.VarChar, 20, "Phone");
      adapter.UpdateCommand.Parameters.Add("@AreaSize",
      OleDbType.Integer, 5, "AreaSize");
      adapter.UpdateCommand.Parameters.Add("@IsWorking",
      OleDbType.Integer, 5, "IsWorking");
      adapter.UpdateCommand.Parameters.Add("@SurName",
          OleDbType.VarChar, 20, "SurName");
      adapter.UpdateCommand.Parameters.Add("@FatherName",
          OleDbType.VarChar, 20, "FatherName");
      adapter.UpdateCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum");

      adapter.InsertCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum");
      adapter.InsertCommand.Parameters.Add("@FirstName",
          OleDbType.VarChar, 20, "FirstName");
      adapter.InsertCommand.Parameters.Add("@Phone",
          OleDbType.VarChar, 20, "Phone");
      adapter.InsertCommand.Parameters.Add("@AreaSize",
      OleDbType.Integer, 5, "AreaSize");
      adapter.InsertCommand.Parameters.Add("@IsWorking",
      OleDbType.Integer, 5, "IsWorking");
      adapter.InsertCommand.Parameters.Add("@SurName",
          OleDbType.VarChar, 20, "SurName");
      adapter.InsertCommand.Parameters.Add("@FatherName",
          OleDbType.VarChar, 20, "FatherName");

      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter AreaStructureAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM AreaStructure", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO AreaStructure (AreaStructureId, StructureSize, MaterialId, PurposeId, AreaNum, StructureDescription) " +
        "VALUES (?, ?, ?, ?, ?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE AreaStructure SET StructureSize = ?, MaterialId = ?, PurposeId = ?, AreaNum = ?, StructureDescription = ?" +
        "WHERE AreaStructureId = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM AreaStructure WHERE AreaStructureId = ?");

      adapter.DeleteCommand.Parameters.Add("@AreaStructureId",
      OleDbType.Integer, 5, "AreaStructureId").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@StructureSize",
          OleDbType.Integer, 5, "StructureSize");
      adapter.UpdateCommand.Parameters.Add("@MaterialId",
          OleDbType.Integer, 5, "MaterialId");
      adapter.UpdateCommand.Parameters.Add("@PurposeId",
      OleDbType.Integer, 5, "PurposeId");
      adapter.UpdateCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum");
      adapter.UpdateCommand.Parameters.Add("@StructureDescription",
          OleDbType.VarChar, 20, "StructureDescription");
      adapter.UpdateCommand.Parameters.Add("@AreaStructureId",
          OleDbType.Integer, 5, "AreaStructureId");

      adapter.InsertCommand.Parameters.Add("@AreaStructureId",
          OleDbType.Integer, 5, "AreaStructureId");
      adapter.InsertCommand.Parameters.Add("@StructureSize",
          OleDbType.Integer, 5, "StructureSize");
      adapter.InsertCommand.Parameters.Add("@MaterialId",
          OleDbType.Integer, 5, "MaterialId");
      adapter.InsertCommand.Parameters.Add("@PurposeId",
      OleDbType.Integer, 5, "PurposeId");
      adapter.InsertCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum");
      adapter.InsertCommand.Parameters.Add("@StructureDescription",
          OleDbType.VarChar, 20, "StructureDescription");
      

      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter StructureMaterialAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM StructureMaterial", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO StructureMaterial (StructureMaterialId, Material) " +
        "VALUES (?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE StructureMaterial SET Material = ?" +
        "WHERE StructureMaterialId = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM StructureMaterial WHERE StructureMaterialId = ?");

      adapter.DeleteCommand.Parameters.Add("@StructureMaterialId",
      OleDbType.Integer, 5, "StructureMaterialId").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@Material",
          OleDbType.VarChar, 20, "Material");
      adapter.UpdateCommand.Parameters.Add("@StructureMaterialId",
          OleDbType.Integer, 5, "StructureMaterialId");

      adapter.InsertCommand.Parameters.Add("@StructureMaterialId",
          OleDbType.Integer, 5, "StructureMaterialId");
      adapter.InsertCommand.Parameters.Add("@Material",
          OleDbType.VarChar, 20, "Material");


      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter StructurePurposeAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM StructurePurpose", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO StructurePurpose (StructurePurposeId, Purpose) " +
        "VALUES (?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE StructurePurpose SET Purpose = ?" +
        "WHERE StructurePurposeId = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM StructurePurpose WHERE StructurePurposeId = ?");

      adapter.DeleteCommand.Parameters.Add("@StructurePurposeId",
      OleDbType.Integer, 5, "StructurePurposeId").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@Purpose",
          OleDbType.VarChar, 20, "Purpose");
      adapter.UpdateCommand.Parameters.Add("@StructurePurposeId",
          OleDbType.Integer, 5, "StructurePurposeId");

      adapter.InsertCommand.Parameters.Add("@StructurePurposeId",
          OleDbType.Integer, 5, "StructurePurposeId");
      adapter.InsertCommand.Parameters.Add("@Purpose",
          OleDbType.VarChar, 20, "Purpose");


      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter SteaderPaymentsAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM SteaderPayments", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO SteaderPayments (AreaNum, Arrears) " +
        "VALUES (?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE SteaderPayments SET Arrears = ?" +
        "WHERE AreaNum = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM SteaderPayments WHERE AreaNum = ?");

      adapter.DeleteCommand.Parameters.Add("@AreaNum",
      OleDbType.Integer, 5, "AreaNum").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@Arrears",
          OleDbType.VarChar, 20, "Arrears");
      adapter.UpdateCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");

      adapter.InsertCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");
      adapter.InsertCommand.Parameters.Add("@Arrears",
          OleDbType.VarChar, 20, "Arrears");


      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter TaxTypeAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM TaxType", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO TaxType (TaxTypeId, Description, PaymentSize) " +
        "VALUES (?, ?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE TaxType SET Description = ?, PaymentSize = ?" +
        "WHERE TaxTypeId = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM TaxType WHERE TaxTypeId = ?");

      adapter.DeleteCommand.Parameters.Add("@TaxTypeId",
      OleDbType.Integer, 5, "TaxTypeId").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@Description",
          OleDbType.VarChar, 20, "Description");
      adapter.UpdateCommand.Parameters.Add("@PaymentSize",
          OleDbType.Integer, 5, "PaymentSize");
      adapter.UpdateCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");

      adapter.InsertCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");
      adapter.InsertCommand.Parameters.Add("@Description",
          OleDbType.VarChar, 20, "Description");
      adapter.InsertCommand.Parameters.Add("@PaymentSize",
          OleDbType.Integer, 5, "PaymentSize");


      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter MonthTaxAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM MonthTax", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO MonthTax (MonthTaxId, Arrears, AreaNum, TaxTypeId) " +
        "VALUES (?, ?, ?, ?)");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE MonthTax SET Arrears = ?, AreaNum = ?, TaxTypeId = ?" +
        "WHERE MonthTaxId = ?");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM MonthTax WHERE MonthTaxId = ?");

      adapter.DeleteCommand.Parameters.Add("@MonthTaxId",
      OleDbType.Integer, 5, "MonthTaxId").SourceVersion = DataRowVersion.Original;

      adapter.UpdateCommand.Parameters.Add("@Arrears",
          OleDbType.Integer, 5, "Arrears");
      adapter.UpdateCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");
      adapter.UpdateCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");
      adapter.UpdateCommand.Parameters.Add("@MonthTaxId",
          OleDbType.Integer, 5, "MonthTaxId");

      adapter.InsertCommand.Parameters.Add("@MonthTaxId",
          OleDbType.Integer, 5, "MonthTaxId");
      adapter.InsertCommand.Parameters.Add("@Arrears",
          OleDbType.Integer, 5, "Arrears");
      adapter.InsertCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");
      adapter.InsertCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");


      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter CurrenAndPreviousValuesAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM CurrenAndPreviousValues", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO CurrenAndPreviousValues (MonthTaxId, CurrentValue, PreviousValue) " +
        "VALUES (?, ?, ?)");

      adapter.InsertCommand.Parameters.Add("@MonthTaxId",
          OleDbType.Integer, 5, "MonthTaxId");
      adapter.InsertCommand.Parameters.Add("@CurrentValue",
          OleDbType.Integer, 5, "CurrentValue");
      adapter.InsertCommand.Parameters.Add("@PreviousValue",
          OleDbType.Integer, 5, "PreviousValue");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE CurrenAndPreviousValues SET CurrentValue = ?, PreviousValue = ?" +
        "WHERE MonthTaxId = ?");

      adapter.UpdateCommand.Parameters.Add("@CurrentValue",
          OleDbType.Integer, 5, "CurrentValue");
      adapter.UpdateCommand.Parameters.Add("@PreviousValue",
          OleDbType.Integer, 5, "PreviousValue");
      adapter.UpdateCommand.Parameters.Add("@MonthTaxId",
          OleDbType.Integer, 5, "MonthTaxId");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM CurrenAndPreviousValues WHERE MonthTaxId = ?");

      adapter.DeleteCommand.Parameters.Add("@MonthTaxId",
      OleDbType.Integer, 5, "MonthTaxId").SourceVersion = DataRowVersion.Original;

      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter YearTaxAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM YearTax", connection);

      adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

      adapter.InsertCommand = new OleDbCommand(
        "INSERT INTO YearTax (YearTaxId, Arrears, AreaNum, TaxTypeId) " +
        "VALUES (?, ?, ?, ?)");

      adapter.InsertCommand.Parameters.Add("@YearTaxId",
          OleDbType.Integer, 5, "YearTaxId");
      adapter.InsertCommand.Parameters.Add("@Arrears",
          OleDbType.Integer, 5, "Arrears");
      adapter.InsertCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");
      adapter.InsertCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");

      adapter.UpdateCommand = new OleDbCommand(
        "UPDATE YearTax SET Arrears = ?, AreaNum = ?, TaxTypeId = ?" +
        "WHERE YearTaxId = ?");

      adapter.UpdateCommand.Parameters.Add("@Arrears",
          OleDbType.Integer, 5, "Arrears");
      adapter.UpdateCommand.Parameters.Add("@AreaNum",
          OleDbType.Integer, 5, "AreaNum");
      adapter.UpdateCommand.Parameters.Add("@TaxTypeId",
          OleDbType.Integer, 5, "TaxTypeId");
      adapter.UpdateCommand.Parameters.Add("@YearTaxId",
          OleDbType.Integer, 5, "YearTaxId");

      adapter.DeleteCommand = new OleDbCommand(
        "DELETE FROM YearTax WHERE YearTaxId = ?");

      adapter.DeleteCommand.Parameters.Add("@YearTaxId",
      OleDbType.Integer, 5, "YearTaxId").SourceVersion = DataRowVersion.Original;

      adapter.InsertCommand.Connection = connection;
      adapter.UpdateCommand.Connection = connection;
      adapter.DeleteCommand.Connection = connection;

      return adapter;
    }

    public static OleDbDataAdapter ArrearsSumAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT Sum(Arrears) AS ArrearsSum FROM YearTax", connection);

      return adapter;
    }

    public static OleDbDataAdapter PaidSumAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT SUM(TaxType.PaymentSize - YearTax.Arrears) AS PaymentsPaid FROM YearTax INNER JOIN TaxType ON YearTax.TaxTypeId = TaxType.TaxTypeId", connection);

      return adapter;
    }

    public static OleDbDataAdapter WorstDebtorsAdapter(OleDbConnection connection)
    {
      var adapter = new OleDbDataAdapter("SELECT * FROM (SELECT HomeSteader.Surname, YearTax.Arrears, TaxType.Description FROM HomeSteader INNER JOIN (YearTax INNER JOIN TaxType ON YearTax.TaxTypeId = TaxType.TaxTypeId) ON YearTax.AreaNum = HomeSteader.AreaNum ORDER BY YearTax.Arrears DESC) WHERE ROWNUM <=3", connection);

      return adapter;
    }

    public static OleDbDataAdapter HomeSteaderYearTaxesAdapter(OleDbConnection connection, string steaderName)
    {
      var adapter = new OleDbDataAdapter("SELECT HomeSteader.Surname, YearTax.Arrears, TaxType.Description FROM HomeSteader INNER JOIN (YearTax INNER JOIN TaxType ON YearTax.TaxTypeId = TaxType.TaxTypeId) ON YearTax.AreaNum = HomeSteader.AreaNum WHERE SurName = '" + steaderName + "'", connection);

      return adapter;
    }
  }
}
